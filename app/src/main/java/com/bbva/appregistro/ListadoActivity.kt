package com.bbva.appregistro


import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.ilhasoft.support.validation.Validator
import com.bbva.appregistro.adapters.DogsAdapter
import com.bbva.appregistro.databinding.ActivityListadoBinding
import com.bbva.appregistro.models.APIService
import com.bbva.appregistro.models.DogsResponse
import com.bbva.appregistro.utils.AlertDialogCustom
import com.bbva.appregistro.utils.ToolbarActivity
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.jetbrains.anko.yesButton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ListadoActivity : ToolbarActivity(), Validator.ValidationListener {
    lateinit var imagesPuppies:List<String>
    lateinit var dogsAdapter: DogsAdapter
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityListadoBinding>(this, R.layout.activity_listado)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inicializarToolbar(binding.barraP, "Listado Pet's", 0)

        binding.btnSearch.setOnClickListener {
            searchByName(binding.searchBreed.text.toString().toLowerCase())
        }
    }

    private fun searchByName(query: String) {
        doAsync {
            val call =
                getRetrofit().create(APIService::class.java).getCharacterByName("$query/images")
                    .execute()

            uiThread {
                if (call.isSuccessful) {
                    val puppies = call.body() as DogsResponse
                    if (puppies.status == "success") {
                        initCharacter(puppies)
                    } else {
                        showErrorDialog()
                    }
                } else {
                    AlertDialogCustom.createAlert(this@ListadoActivity,"¡Lo sentimos!","No hay datos que coincidan con su búsqueda.")
                }
            }
        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/breed/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun initCharacter(puppies: DogsResponse) {
        if(puppies.status == "success"){
            imagesPuppies = puppies.images
        } else {
            alert("Sin datos.") {
                yesButton { }
            }.show()
        }
        dogsAdapter =
            DogsAdapter(this, imagesPuppies)
        binding.rvDogs.setHasFixedSize(true)
        binding.rvDogs.layoutManager = LinearLayoutManager(this)
        binding.rvDogs.adapter = dogsAdapter

    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }

    override fun onValidationSuccess() {
        Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@ListadoActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}