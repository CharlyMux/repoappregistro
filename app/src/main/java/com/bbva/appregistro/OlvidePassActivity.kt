package com.bbva.appregistro

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.appregistro.databinding.ActivityOlvidePassBinding
import com.bbva.appregistro.utils.AlertDialogCustom
import com.bbva.appregistro.utils.ToolbarActivity

class OlvidePassActivity : ToolbarActivity(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityOlvidePassBinding>(this, R.layout.activity_olvide_pass)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inicializarToolbar(binding.barraP, "Olvide Contraseña", 0)

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnRecuperar.id -> {
                    validator!!.toValidate()
/*                    if (validator.validate()==true) {
                        intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                    }*/
                }
            }
        }
    }

    override fun onValidationSuccess() {
        //Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
        intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onValidationError() {
        //Toast.makeText(this@OlvidePassActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
        AlertDialogCustom.createAlert(this@OlvidePassActivity, "¡Error!","Debé de colocar los datos")
    }
}