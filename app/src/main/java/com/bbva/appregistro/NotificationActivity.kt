package com.bbva.appregistro

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bbva.appregistro.adapters.NotificationAdapter
import com.bbva.appregistro.databinding.ActivityNotificationBinding
import com.bbva.appregistro.models.NotificationModel
import com.bbva.appregistro.utils.ToolbarActivity

class NotificationActivity : ToolbarActivity() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityNotificationBinding>(this, R.layout.activity_notification)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarToolbar(binding.barraP, "Notificaciones", 1)

        //adding a layoutmanager
        binding.rvNotification.layoutManager = LinearLayoutManager(this)

        //crating an array list to store users using the data class user
        val notifications = ArrayList<NotificationModel>()

        //adding some dummy data to the list
        notifications.add(
            NotificationModel(
                "Título de Notificación 1",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vulputate libero viverra ligula laoreet semper.",
                "R"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 2",
                "Donec vulputate libero viverra ligula laoreet semper.Lorem ipsum dolor sit amet, consectetur adipiscing elit. ",
                "R"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 3",
                "Vestibulum arcu orci, hendrerit ac odio et, rhoncus aliquet lacus. Donec a nisl auctor, elementum urna sed.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 4",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 5",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 6",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 7",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 8",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 9",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 11",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 12",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 13",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 14",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 15",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 16",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )
        notifications.add(
            NotificationModel(
                "Título de Notificación 17",
                "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.",
                "L"
            )
        )

        //creating our adapter
        val adapter = NotificationAdapter(
            this,
            notifications
        )

        //adding the adapter to recyclerview
        binding.rvNotification.adapter = adapter
    }
}