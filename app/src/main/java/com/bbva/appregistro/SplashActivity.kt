package com.bbva.appregistro
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.bbva.appregistro.utils.SharedPreference


class SplashActivity: AppCompatActivity() ,
    Runnable{

    private lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPreference = SharedPreference(this)
        setContentView(R.layout.activity_splash)

        //simularCarga()
        if (sharedPreference.getValueString("UsrSes") != null) {
            intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        } else {
            simularCarga()
        }

    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus)
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    private fun simularCarga(){
        handler = Handler()
        handler.postDelayed(this, 3000)
    }

    //simulación de carga
    override fun run() {
        intent = Intent(this, MainActivity::class.java)  //MainActivity
        startActivity(intent)
        finish()


    }

}