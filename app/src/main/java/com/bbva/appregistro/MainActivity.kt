package com.bbva.appregistro

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.appregistro.databinding.ActivityMainBinding
import com.bbva.appregistro.utils.AlertDialogCustom
import com.bbva.appregistro.utils.SharedPreference
import com.bbva.appregistro.utils.ToolbarActivity

class MainActivity : ToolbarActivity(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val sharedPreference = SharedPreference(this)
//        var strUsr=""

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnIniciar.id -> {
                    validator!!.toValidate()
/*                    if (validator.validate()==true) {
                        strUsr = binding.txtUsrIni.text.toString()
                        sharedPreference.save("UsrSes", strUsr)
                        intent = Intent(applicationContext, MenuActivity::class.java)
                        startActivity(intent)
                    }*/
                }
                binding.txtOlvPass.id -> {
                    intent = Intent(applicationContext, OlvidePassActivity::class.java)
                    startActivity(intent)
                }
                binding.txtRegistrar.id -> {
                    intent = Intent(applicationContext, RegistroActivity::class.java)
                    startActivity(intent)
                }
            }
        }

    }

    override fun onValidationSuccess() {
        //Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
        val sharedPreference = SharedPreference(this)
        var strUsr=""
        strUsr = binding.txtUsrIni.text.toString()
        sharedPreference.save("UsrSes", strUsr)
        intent = Intent(applicationContext, MenuActivity::class.java)
        startActivity(intent)
    }

    override fun onValidationError() {
        //Toast.makeText(this@MainActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
        AlertDialogCustom.createAlert(this@MainActivity, "¡Error!","Debé de colocar los datos")
    }
}