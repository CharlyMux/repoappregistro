package com.bbva.appregistro.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bbva.appregistro.utils.AlertDialogCustom
import com.bbva.appregistro.models.NotificationModel
import com.bbva.appregistro.R
import com.bbva.appregistro.databinding.ItemNotificationBinding

class NotificationAdapter(var context: Context, var list: ArrayList<NotificationModel>):
        RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    class ViewHolder(var bind: ItemNotificationBinding): RecyclerView.ViewHolder(bind.root)
    override fun getItemCount(): Int = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater  = LayoutInflater.from(parent.context)
        val bind = ItemNotificationBinding.inflate(inflater,parent,false)
        return ViewHolder(bind)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind.notification = list[position]
        holder.bind.circle = if(list[position].status == "L"){
            ContextCompat.getDrawable(context,
                R.drawable.circle_gray
            )
        }else{
            ContextCompat.getDrawable(context,
                R.drawable.circle_blue
            )
        }
        holder.bind.root.setOnClickListener {
            Log.d("CLICK===>",list[position].title)
            AlertDialogCustom.createAlert(
                context,
                "" + list[position].title,
                list[position].message
            )
        }
    }
}