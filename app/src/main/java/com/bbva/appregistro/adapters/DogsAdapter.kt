package com.bbva.appregistro.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bbva.appregistro.utils.AlertDialogCustom
import com.bbva.appregistro.databinding.ItemDogBinding
import com.bbva.appregistro.utils.fromUrl


class DogsAdapter(var contexto: Context, val images: List<String>) : RecyclerView.Adapter<DogsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemDogBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(images[position])
        holder.binding.setClickListener {
            when (it!!.id) {
                holder.binding.ivDog.id -> {
                    AlertDialogCustom.createAlert(
                        contexto,
                        "Toco la foto",
                        images[position]
                    )
                }
            }
        }
    }

    inner class ViewHolder(val binding: ItemDogBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(image: String) {
            binding.razaUrl=image
            binding.ivDog.fromUrl(image)
            binding.executePendingBindings()
        }
    }
}
