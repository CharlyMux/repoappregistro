package com.bbva.appregistro

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.appregistro.databinding.ActivityFirmaBinding
import com.bbva.appregistro.utils.AlertDialogCustom
import com.bbva.appregistro.utils.TimePickerFragment
import com.bbva.appregistro.utils.ToolbarActivity
import com.github.gcacace.signaturepad.views.SignaturePad
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

@SuppressLint("ByteOrderMark")
class FirmaActivity : ToolbarActivity(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityFirmaBinding>(this, R.layout.activity_firma)
    }

    companion object {
        val TAG = "PermissionDemo"
        private val REQUEST_PERMISSION = 2000
        private const val REQUEST_INTERNET = 200
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firma)
        inicializarToolbar(binding.barraP, "Check In", 0)
        revisaPermiso()

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)

        binding.txtHoraEnt.isEnabled=false
        binding.setClickListener {
            when (it!!.id) {
                binding.btnRegistrar.id -> {
                    validator!!.toValidate()
                    //val signatureBitmap: Bitmap = binding.signaturePad.getTransparentSignatureBitmap()
                    val signatureBitmap: Bitmap = binding.signaturePad.transparentSignatureBitmap
                    //if (addJpgSignatureToGallery(signatureBitmap)) {
                    if(!saveImage(signatureBitmap).isNullOrEmpty()){
                        Toast.makeText(this, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();//
                        intent = Intent(applicationContext, MenuActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(
                                this,
                                "Unable to store the signature",
                                Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                binding.llyHora.id->{
                    showTimePickerDialog()
                }
            }
        }

        binding.signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() { //Toast.makeText(SignActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            override fun onSigned() {
                binding.btnRegistrar.setEnabled(true)
            }

            override fun onClear() {
                binding.btnRegistrar.setEnabled(false)

            }
        })

/*        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnRegistrar.id -> {
                    validator!!.toValidate()

                }
            }
        }*/
    }
    private fun showTimePickerDialog() {
        val timePicker =
            TimePickerFragment { onTimeSelected(it) }
        timePicker.show(supportFragmentManager, "timePicker")
    }
    private fun onTimeSelected(time: String) {
        binding.txtHoraEnt.text = "$time"
    }
    fun revisaPermiso(){
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_INTERNET
            )
            Log.i(TAG, "Pide permiso")
        }

    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_INTERNET -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Si dio permiso")
            } else {
                Log.i(TAG, "No dio permiso")
            }
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
 /*       val wallpaperDirectory = File(
                (Environment.getExternalStorageDirectory()).toString() + "/cognitus"
        )*/
        val wallpaperDirectory = File(
                (Environment.getExternalStorageDirectory()).toString()
        )
        // build the directory structure
        Log.d("file", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                    wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                    this,
                    arrayOf(f.getPath()),
                    arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }

    override fun onValidationSuccess() {
        //Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
        intent = Intent(applicationContext, MenuActivity::class.java)
        startActivity(intent)
    }

    override fun onValidationError() {
        //Toast.makeText(this@FirmaActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
        AlertDialogCustom.createAlert(this@FirmaActivity, "¡Error!","Debé de colocar los datos")
    }

}