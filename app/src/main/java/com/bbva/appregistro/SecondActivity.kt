package com.bbva.appregistro

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.appregistro.databinding.ActiviteSecondBinding
import com.bbva.appregistro.utils.ToolbarActivity


class SecondActivity : ToolbarActivity(), Validator.ValidationListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActiviteSecondBinding>(this, R.layout.activite_second)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inicializarToolbar(binding.barraP, "NOTICIAS", 1)
        binding.barraP.ivBtn1.setOnClickListener {
            Toast.makeText(this, "Botón de barra ", Toast.LENGTH_LONG).show()
        }

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnEva.id -> {
                    validator!!.toValidate()
                }
            }
        }
    }

    override fun onValidationSuccess() {
        Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@SecondActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}