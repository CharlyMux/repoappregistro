package com.bbva.appregistro

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.appregistro.databinding.ActivityMenuBinding
import com.bbva.appregistro.utils.SharedPreference
import com.bbva.appregistro.utils.ToolbarActivity

class MenuActivity : ToolbarActivity(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMenuBinding>(this, R.layout.activity_menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPreference = SharedPreference(this)

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.txtNoticias.id -> {
                    Toast.makeText(this, "Noticias ", Toast.LENGTH_LONG).show()
                }
                binding.txtChekIn.id -> {
                    intent = Intent(applicationContext, FirmaActivity::class.java)
                    startActivity(intent)
                }
                binding.txtNotificaciones.id -> {
                    //Toast.makeText(this, "Notificaciones ", Toast.LENGTH_LONG).show()
                    intent = Intent(applicationContext, NotificationActivity::class.java)
                    startActivity(intent)
                }
                binding.txtPeril.id -> {
                    intent = Intent(applicationContext, PerfilActivity::class.java)
                    startActivity(intent)
                }
                binding.txtTareas.id -> {
                    intent = Intent(applicationContext, ListadoActivity::class.java)
                    startActivity(intent)
                }

                binding.llyCerrarSes.id -> {
                    sharedPreference.clearSharedPreference()
                    intent = Intent(applicationContext, MainActivity::class.java)
                    startActivity(intent)
                }
            }
        }
    }

    override fun onValidationSuccess() {
        Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@MenuActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}