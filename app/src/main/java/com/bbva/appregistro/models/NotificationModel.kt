package com.bbva.appregistro.models

data class NotificationModel(val title: String, val message: String,val status:String)