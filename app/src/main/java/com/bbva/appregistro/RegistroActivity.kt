package com.bbva.appregistro

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.appregistro.databinding.ActivityRegistroBinding
import com.bbva.appregistro.utils.AlertDialogCustom
import com.bbva.appregistro.utils.ToolbarActivity

class RegistroActivity : ToolbarActivity(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityRegistroBinding>(this, R.layout.activity_registro)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inicializarToolbar(binding.barraP, "Registro", 0)

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnRegistrar.id -> {
                    validator!!.toValidate()
  /*                  if (validator.validate()==true && binding.chkTermCond.isActivated()==true ) {
                        intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                    }*/
                }
                binding.chkTermCond.id -> {
                    //Toast.makeText(this, "Valor Check ", Toast.LENGTH_LONG).show()
                    if (binding.chkTermCond.isChecked)
                        AlertDialogCustom.createAlert(this, "Valor Check", "Checado")
                    else
                        AlertDialogCustom.createAlert(this, "Valor Check", "No checado")
                }
            }
        }
    }

    override fun onValidationSuccess() {
        if (binding.chkTermCond.isChecked){
            Toast.makeText(this, "Valores Registrados ", Toast.LENGTH_LONG).show()
            intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }
        else
            AlertDialogCustom.createAlert(this, "Terminos y Condiciones", "Se deben de Aceptar los Teminos y condicines")

    }

    override fun onValidationError() {
        //Toast.makeText(this@RegistroActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
        AlertDialogCustom.createAlert(this@RegistroActivity, "¡Error!","Debé de colocar los datos")
    }
}